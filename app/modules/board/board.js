'use strict';

angular.module('myApp.board', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/boards', {
            templateUrl: 'modules/board/board.html',
            controller: 'BoardCtrl',
            controllerAs: 'BoardCtrl',
        });
        $routeProvider.when('/boards/create', {
            templateUrl: 'modules/board/boardcreate.html',
            controller: 'BoardCreateCtrl',
            controllerAs: 'BoardCreateCtrl',
        });
        $routeProvider.when('/boards/:board_id/edit', {
            templateUrl: 'modules/board/boardedit.html',
            controller: 'BoardEditCtrl',
            controllerAs: 'BoardEditCtrl',
        });
        $routeProvider.when('/boards/:board_id/ideas/create', {
            templateUrl: 'modules/board/ideacreate.html',
            controller: 'IdeaCreateCtrl',
            controllerAs: 'IdeaCreateCtrl',
        });
    }])

    .controller('BoardCtrl', ['$http', 'urls', function ($http, urls) {
        var self = this;

        self.error = "";
        self.boards = [];

        /**
         * get all boards
         * @returns {string}
         */
        self.getBoards = function () {
            $http.get(urls.api.boards)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            self.boards = response.data;
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };
        self.getBoards();


        self.deleteBoard = function (board_id, index) {
            $http.delete(urls.api.boards + board_id, {})
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            self.boards.splice(index, 1);
                            alert("Board removed");
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };

    }])
    .controller('BoardCreateCtrl', ['$http', 'urls', '$location', function ($http, urls, $location) {
        var self = this;

        self.categories = [];
        self.board = {};

        /**
         * get categories
         */
        self.getCategories = function () {
            $http.get(urls.api.categories)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            self.categories = response.data;
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };
        self.getCategories();

        /**
         * save form
         * @param board
         */
        self.save = function (board) {
            $http.post(urls.api.boards, board)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            alert("Saved board");
                            $location.path("/boards");
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };


    }])
    .controller('BoardEditCtrl', ['$scope', '$http', 'urls', '$location', '$routeParams', function ($scope, $http, urls, $location, $routeParams) {
        var self = this;

        self.categories = [];
        self.board_id = $routeParams.board_id;
        $scope.board = {};

        /**
         * get all board detail
         * @returns {string}
         */
        self.getBoard = function () {
            $http.get(urls.api.boards + self.board_id)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            self.board = response.data;
                            self.board.category_id = self.board.category.id;

                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };
        self.getBoard();

        /**
         * get categories
         */
        self.getCategories = function () {
            $http.get(urls.api.categories)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            self.categories = response.data;
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };
        self.getCategories();

        /**
         * save form
         * @param board
         */
        self.save = function (board) {
            $http.put(urls.api.boards + self.board_id, board)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            alert("Saved board");
                            $location.path("/boards");
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };


    }])
    .controller('IdeaCreateCtrl', ['$http', 'urls', '$location', '$routeParams', function ($http, urls, $location, $routeParams) {
        var self = this;
        self.idea = {};
        self.board_id = $routeParams.board_id;

        /**
         * save form
         * @param idea
         */
        self.save = function (idea) {
            idea.board_id = self.board_id;
            $http.post(urls.api.ideas, idea)
                .then(
                    function (res) {
                        var response = res.data;
                        if (response.success) {
                            alert("Saved idea");
                            $location.path("/boards");
                        } else {
                            alert("An error has occurred");
                        }
                    },
                    function (error) {
                        alert("An error has occurred");
                    }
                );
        };


    }]);
