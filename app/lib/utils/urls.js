'use strict';

var host='http://127.0.0.1:8001';

angular.module('myApp.urls', [])
    .constant('urls', {
        'api':{
            'boards': host+'/api/boards/',
            'ideas': host+'/api/ideas/',
            'categories': host+'/api/categories/'
        }

    });
